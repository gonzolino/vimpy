" Load pathogen
filetype off
call pathogen#runtime_append_all_bundles()
call pathogen#helptags()

" Set map leader key
let mapleader=","

" Set pep8 mapping
let g:pep8_map='<leader>8'

" Set up folding for python
set foldmethod=indent
set foldlevel=99

" Different key bindings for plugins
map <Leader>td <Plug>TaskList
map <Leader>g :GundoToggle<CR>
map <Leader>n :NERDTreeToggle<CR>
map <Leader>j :RopeGoToDefinition<CR>
map <Leader>r :RopeRename<CR>
map <Leader>a <Esc>:Ack!

" Enable code dompletion
au FileType python set omnifunc = pythoncomplete#Complete
let g:superTabDefaultCompletionType = "context"
set completeopt=menuone,longest,preview

" Add fugitive infos to statusline
set statusline=%{fugitive#statusline()}


set hidden                      " switch buffers without writing to disk
set nocompatible                " disable compatibility to vi
set encoding=utf-8              " utf-8!
set smartindent                 " copy indent from current line when starting a new line
set tabstop=4                   " four spaces for a <TAB>
set expandtab                   " change all tabs into spaces
set shiftwidth=4                " Four spaces to use for each step of (auto)indent.
set shiftround                  " Round indent to multiple of 'shiftwidth'
set incsearch                   " do incremental searching
set hlsearch                    " highlight found search term
set showcmd                     " display incomplete commands
set number                      " show linenumbers
set mouse=a                     " activate mouse in terminal
set visualbell t_vb=            " disable error beeb
set ignorecase                  " ignore case when searching
set smartcase                   " only ignore case when typing lower case
syntax on                       " enable syntac highlighting
filetype on			" try to detect filetype
filetype plugin indent on	" load indent file for detected filetype
